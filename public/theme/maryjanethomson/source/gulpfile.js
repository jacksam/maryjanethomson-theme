var gulp            = require('gulp'),
    postcss         = require('gulp-postcss'),
    lost            = require('lost'),
    concat          = require('gulp-concat'),
    uglify          = require('gulp-uglify'),
    sass            = require('gulp-sass'),
    cssnano         = require('gulp-cssnano'),
    autoprefixer    = require('autoprefixer'),
    rename          = require('gulp-rename'),
    plumber         = require('gulp-plumber'),
    babel           = require('gulp-babel');

gulp.task('build-css', function () {
    var processors = [
        lost,
        autoprefixer({
            browsers: ['last 3 version']
        })
    ];
    return gulp.src('scss/main.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(postcss(processors))
        .pipe(cssnano())
        .pipe(gulp.dest('../css'))
});

gulp.task('build-photos-scripts', function () {
    return gulp.src('js/photos/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('photos.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../js'))
        .pipe(plumber());
});

gulp.task('build-words-scripts', function () {
    return gulp.src('js/words/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('words.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../js'))
        .pipe(plumber());
});

gulp.task('build-plugins', function () {
    return gulp.src('js/plugins/*.js')
        .pipe(concat('plugins.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../js'))
        .pipe(plumber());
});

gulp.task('build-assets', function () {
    return gulp.src('assets/**')
        .pipe(plumber())
        .pipe(gulp.dest('../assets'));
});

gulp.task('build-fonts', function () {
    return gulp.src('fonts/**')
        .pipe(plumber())
        .pipe(gulp.dest('../fonts'));
});

gulp.task('watch', function () {
    gulp.watch(['scss/**'], ['build-css']);
    gulp.watch(['js/**'], ['build-plugins', 'build-photos-scripts', 'build-words-scripts']);
});


gulp.task('default', ['build-css', 'build-plugins', 'build-photos-scripts', 'build-words-scripts', 'build-fonts', 'build-assets', 'watch']);

gulp.task('deploy', ['build-css', 'build-plugins', 'build-photos-scripts', 'build-words-scripts', 'build-fonts', 'build-assets']);