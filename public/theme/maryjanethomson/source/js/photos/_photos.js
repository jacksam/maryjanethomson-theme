var photos = new Vue({
    el: '#photos',
    beforeCreate: function () {
        fetch('/data/photos?page[size]=500').then(function(response) {
            return response.json();
        }).then(function(json) {
            
            var photoData        = json.data,
                filteredArray    = [];

                

            Object.keys(photoData).forEach(function (key) {
                var id                  = photoData[key].id,
                    title               = photoData[key].attributes.title,
                    slug                = photoData[key].attributes.slug,
                    imageFile           = photoData[key].attributes.image.file,
                    setCategory         = photoData[key].attributes.taxonomy.groups,
                    categoryString      = Object.keys(setCategory)[0],
                    category            = capitalize(categoryString.split("/").pop()),
                    image               = '/thumbs/500x500r/' + imageFile,
                    largeImage          = '/thumbs/1920x1920r/' + imageFile,
                    caption             = ("<span class='slug'>" + slug + "</span>" + "<span class='caption'>" + title + "</span>")

                filteredArray.push({
                    id: id,
                    title: title,
                    slug: slug,
                    caption: caption,
                    image: image,
                    largeImage: largeImage,
                    category: category
                })
                
            });

            console.log(filteredArray);

            photos.allPhotos = filteredArray;
            if(document.location.search.length) {
                var query       = getSlug('photo'),
                    category,
                    photoElement;
                var getImage = photos.allPhotos.filter(function(slug){
                    return slug.slug == query;
                });
                category = getImage[0].category;
                photos.loadPhotos(null, category);
                setTimeout(function(){
                    photoElement = document.querySelector(".photos__gallery--element[data-slug=" + query + "]");
                    photoElement.click()
                }, 500);
            }
        });
    },
    data: function() {
        return {
            allPhotos: [],
            category: []
        };
    },
    methods: {
        loadPhotos: function(event, category){
            var windowHeight            = window.innerHeight,
                galleryElements         = document.querySelectorAll('.photos__gallery--element'),
                allPhotos               = photos.allPhotos,
                photoGallerySection     = document.querySelector('.photos__gallery');

            if(event != null){
                var button = event.target;
            }

            photoGallerySection.setAttribute('data-state','hidden');

            for (var i=0; i < galleryElements.length; i++) {
                galleryElements[i].setAttribute("data-view", "hidden");
            }

            var getImagesByCategory = allPhotos.filter(function(categories){
                return categories.category == category;
            });

            console.log(getImagesByCategory);

            photos.category = [];
            photos.category = getImagesByCategory;

            photoGallerySection.setAttribute('data-state','visible');

            setTimeout(function(){
                setUpGallery();
                inView.threshold(0.25);
                inView('.photos__gallery--element')
                .on('enter', function(el) {
                    var galleryElement = el;
                    galleryElement.setAttribute('data-view', 'visible');
                });
                window.scrollBy({
                    top: windowHeight,
                    left: 0,
                    behavior: 'smooth'
                });
            }, 500);
        }
    },
    delimiters: ['${', '}']
});


var bricklayer = new Bricklayer(document.querySelector('.bricklayer'))

var mainMenu            = document.querySelector('.navigation');

function setUpGallery() {
    baguetteBox.run('.photos__gallery', {
        animation: 'fadeIn',
        captions: true,
        async: true,
        noScrollbars: true,
        afterShow: function(){
            var galleryCloseButton  = document.getElementById("close-button");
            galleryCloseButton.addEventListener("click", closeGallery);
            mainMenu.setAttribute('data-state','gallery');
        },
        onChange: function(currentIndex){
            var index           = currentIndex,
                url             = window.location.host,
                slugElement     = document.getElementById('baguetteBox-figcaption-' + index),
                slug            = slugElement.querySelector('.slug').innerText,
                facebook        = document.querySelector('.share__menu--button.facebook'),
                facebookUrl     = ('https://www.facebook.com/sharer/sharer.php?u=' + encodeURI('https://' + url + '/?photo=' + slug)),
                twitter         = document.querySelector('.share__menu--button.twitter'),
                twitterUrl      = ('https://twitter.com/home?status=' + encodeURI('https://' + url + '/?photo=' + slug)),
                pinterest       = document.querySelector('.share__menu--button.pinterest'),
                pinterestUrl     = ('https://pinterest.com/pin/create/button/?url=&media=' + encodeURI('https://' + url + '/?photo=' + slug)),
                email           = document.querySelector('.share__menu--button.email'),
                emailUrl        = ('mailto:?subject=&amp;body=Check out this photo ' + encodeURI('https://' + url + '/?photo=' + slug));

            history.pushState(null, null, '?photo=' + slug);

            facebook.href    = facebookUrl
            twitter.href     = twitterUrl
            pinterest.href   = pinterestUrl
            email.href       = emailUrl
        },
        afterHide: function(){
            var defaultUrl = window.location.protocol + "//" + window.location.host;
            var galleryCloseButton  = document.getElementById("close-button");
            mainMenu.setAttribute('data-state','default');
            galleryCloseButton.removeEventListener("mousemove", closeGallery);
            history.pushState(null, null, defaultUrl);
        }
    });
}


function closeGallery() {
    mainMenu.setAttribute('data-state','default');
}
