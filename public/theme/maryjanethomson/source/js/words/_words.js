var words = new Vue({
    el: '#words',
    beforeCreate: function () {
        if(document.location.search.length) {
            var query       = getSlug('poem'),
                poem        = document.querySelector('.words__intro--menu ul li a[data-slug=' + query + ']'),
                poemId      = poem.getAttribute('data-id'),
                poemSlug    = poem.getAttribute('data-slug');


            setTimeout(function(){
                words.getWords('load', poemSlug, poemId);
                poem.classList.add('active');
            }, 0);

        }
    },
    data: function() {
        return {
            poem: [],
            stanzas: [],
            colour: []
        };
    },
    methods: {
        getWords: function(type, event, id){
            var link,
                links           = document.querySelectorAll('.words__intro--menu ul li a'),
                windowHeight    = window.innerHeight,
                shareLinks      = document.querySelector('.words__share');

            for (var i = 0, len = links.length; i < len; i++) {
                links[i].classList.remove('active');
            };

            if(type == 'click'){
                link = event.target
                link.classList.add('active');
            }
            else{
                link = event
                var poemLink =  document.querySelector('.words__intro--menu ul li a[data-slug=' + link + ']');
                poemLink.classList.add('active');
            }

            fetch('/data/words/' + id).then(function(response) {
                return response.json();
            }).then(function(json) {
                var wordData        = json.data,
                    filteredArray   = [];

                var id                  = wordData.id,
                    title               = wordData.attributes.title,
                    colour              = wordData.attributes.colour,
                    slug                = wordData.attributes.slug,
                    poemYear            = wordData.attributes.worddate,
                    buyLink             = wordData.attributes.buylink,
                    introduction        = wordData.attributes.introduction,
                    stanzas             = wordData.attributes.blocks;

                filteredArray.push({
                    id: id,
                    title: title,
                    slug: slug,
                    buyLink: buyLink,
                    poemYear: poemYear,
                    introduction: introduction
                });

                words.colour = colour;
                words.poem = filteredArray;
                words.stanzas = stanzas;

                history.pushState(null, null, '?poem=' + slug);

                var url             = window.location.href,
                    facebook        = document.querySelector('.words__share--button.facebook'),
                    facebookUrl     = ('https://www.facebook.com/sharer/sharer.php?u=' + encodeURI(url)),
                    twitter         = document.querySelector('.words__share--button.twitter'),
                    twitterUrl      = ('https://twitter.com/home?status=' + encodeURI(url)),
                    pinterest       = document.querySelector('.words__share--button.pinterest'),
                    pinterestUrl     = ('https://pinterest.com/pin/create/button/?url=&media=' + encodeURI(url)),
                    email           = document.querySelector('.words__share--button.email'),
                    emailUrl        = ('mailto:?subject=&amp;body=Check out this photo ' + encodeURI(url));

                facebook.href    = facebookUrl
                twitter.href     = twitterUrl
                pinterest.href   = pinterestUrl
                email.href       = emailUrl

                shareLinks.setAttribute('data-state', 'visible');

                setTimeout(function(){
                    var stanzaElements = document.querySelectorAll('.words__stanzas--element');
                    for (var i=0; i < stanzaElements.length; i++) {
                        stanzaElements[i].setAttribute("data-view", "hidden");
                    }
                    inView.threshold(0.25);
                    inView('.words__stanzas--element')
                        .on('enter', function(el) {
                            var stanzaElement = el;
                            stanzaElement.setAttribute('data-view', 'visible');
                        });
                    window.scrollBy({
                        top: windowHeight,
                        left: 0,
                        behavior: 'smooth'
                    });
                }, 500);

            });
        }
    },
    delimiters: ['${', '}']
});


var bricklayer = new Bricklayer(document.querySelector('.bricklayer'))